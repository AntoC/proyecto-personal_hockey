# proyecto

## Configuración del proyecto

```
npm install
```

### Compila y recarga en caliente para el desarrollo

```
npm run serve
```

### Compila y minimiza para la producción.

```
npm run build
```

### Limpia y corrige archivos

```
npm run lint
```

### Personalizar la configuración

npm install vue-cookies --save



See [Configuration Reference](https://cli.vuejs.org/config/).

Instalar:

npm install vue-cookies --save

_/REPO GIT/_
https://gitlab.com/AntoC/proyecto-personal_hockey.git

_/COMANDOS GIT/_
Para poder fusionar todos los cambios que se han hecho en el repositorio local trabajando, el comando que se usa es:

PARA SUBIR A EL REPOSITORIO O PARA BAJAR DEL MISMO.

    git push  → subir al repo
    git pull    → Descargar del repo
    git add <filenames>   → Añade al commit el archivo (o archivos)
    git add . → Añade al commit todos los cambios
    git commit -m "mensaje del commit"   → commit. Esto en local
    git status → Muestra el estado en local del repositorio

Para que no nos pida la contraseña todas las veces

Ver consideraciones de seguridad en https://git-scm.com/book/es/v2/Herramientas-de-Git-Almacenamiento-de-credenciales

    git config --global credential.helper cache → Guarda la contraseña durante un tiempo
    git config --global credential.helper store → Guarda la contraseña permanentemente en ~/.git-credentials

git commit (local)
git status (verde o rojo)
git push (para subir)
e <url> → crea una copia en local del repositorio

Cotidianos

    git push  → subir al repo
    git pull    → Descargar del repo
    git add <filenames>   → Añade al commit el archivo (o archivos)
    git add . → Añade al commit todos los cambios
    git commit -m "mensaje del commit"   → commit. Esto en local
    git status → Muestra el estado en local del repositorio

Guardar la contraseña:
git config --global credential.helper store

Cosas que nos han pedido:

**Colores coorporatvos: Azul Royal==5002 RAL==rgb(0, 56, 123);
**Letra: Verdana o Calibri

Esquema:
www.santutxuht.eus (Dominio en propiedad)
**Presentación/Aurkezpena
**Equipos/Taldeak
**Partidos-resultados/Partidak-emaitzak
**Escuela de patín /patin-eskola
**Campus
???Noticias/Berriak( Parece que no quieren de momento este apartado)
???Historia (No han entregado el video ni el texto)
**Contactos/kontaktua
(Hazte socio/ Egizu bazkidea) No se hace. Ponemos forma de contactar
\*\*Enlaces /estekak

Castellano (/ Euskera)
**Responsive
**Carrusel fotos

PARA EL RESPONSIVE

/_ For mobile phones: _/
[class*="col-"] {
width: 100%;
}

@media only screen and (min-width: 600px) {
/_ For tablets: _/
}
@media only screen and (min-width: 768px) {
/_ For desktop: _/
}

/_Consulta de medios para agregar un punto deinterrupcion a 800px:_/

@media screen and (max-width:800px) {
.left, .main, .right {
width:100%; /_ The width is 100%, when the viewport is 800px or smaller --
El ancho es del 100% cuando la ventana grafica es de 800px o menor_/
}
}
Autor: Antonia Cano Redondo
canoredondoa@gmail.com

Federación Vizcaína de Patinaje /

http://www.irristaketa.org/publico/home.asp?idioma=ca

Federación Vasca de Patinaje:

Bizkaiko Irristaketa Federazioa
http://www.irristaketa.org/publico/home.asp?idioma=eu

Federación Vasca de Patinaje

http://fvpatinaje.eus

Euskal Herriko Irristaketa Federazioa

http://fvpatinaje.eus

Real Federación Española de Patinaje

https://fep.es/website/index.asp

para cambiar a otro lenguaje:

<div v-if="$cookies.get('lang')== 'eu'">
      en euskera
      </div>
<div v-if="$cookies.get('lang')== 'es'">
      en castellano
      </div>

      es:{},
      eu:{},

para el menú de Historia:
      <div class="menu_historia">
    <ul id=”button”>
 <li><a href=”#”>Antecedentes </a></li>
 <li><a href=”#”>Creación </a></li>
 <li><a href=”#”>Los años 80 </a></li>
 <li><a href=”#”>Los años 90</a></li>
 <li><a href=”#”>La primera década del siglo XXI </a></li>
 <li><a href=”#”>La segunda década del siglo XXI </a></li>
 </ul>
 </div>


