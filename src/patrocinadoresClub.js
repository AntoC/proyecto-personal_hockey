export default {
    patrocinadores: [
        require("@/assets/Publicidad/Alai.jpg"),
        require("@/assets/Publicidad/Albizu.jpg"),
        require("@/assets/Publicidad/Ana.jpg"),
        require("@/assets/Publicidad/APG.jpg"),
        require("@/assets/Publicidad/Arkupe.jpg"),

        require("@/assets/Publicidad/peñascal-logo.jpg"),

        require("@/assets/Publicidad/Arteburu.jpg"),
        require("@/assets/Publicidad/BacalaosPil-Piliean.jpg"),
        require("@/assets/Publicidad/Baserri.jpg"),
        require("@/assets/Publicidad/Baztertxu.jpg"),
        require("@/assets/Publicidad/Bilbao_Kirolak.jpg"),

        require("@/assets/Publicidad/factoriaF5.png"),

        require("@/assets/Publicidad/Bilbo.jpg"),
        require("@/assets/Publicidad/Boto.jpg"),
        require("@/assets/Publicidad/Brillantes.jpg"),
        require("@/assets/Publicidad/Conorte.jpg"),
        require("@/assets/Publicidad/Dayton.jpg"),

        require("@/assets/Publicidad/2-Pñascal_400x400.png"),

        require("@/assets/Publicidad/El_Norte.jpg"),
        require("@/assets/Publicidad/Flores_Lys.jpg"),
        require("@/assets/Publicidad/Floristeria_Santutxu.jpg"),
        require("@/assets/Publicidad/Garaizar.jpg"),
        require("@/assets/Publicidad/Grafiazkar.jpg"),

        require("@/assets/Publicidad/peñascal-logo.jpg"),

        require("@/assets/Publicidad/Guretxe.jpg"),
        require("@/assets/Publicidad/Hnos_Urrutia.jpg"),
        require("@/assets/Publicidad/Lambrusko.jpg"),
        require("@/assets/Publicidad/Lanbroa.jpg"),
        require("@/assets/Publicidad/Mahats_Herri.jpg"),

        require("@/assets/Publicidad/2-Pñascal_400x400.png"),

        require("@/assets/Publicidad/Moralejo.jpg"),
        require("@/assets/Publicidad/Munoa.jpg"),
        require("@/assets/Publicidad/Norte_y_Sur.jpg"),
        require("@/assets/Publicidad/Peluqueria_Low_Cost.jpg"),
        require("@/assets/Publicidad/PokerStar.jpg"),

        require("@/assets/Publicidad/peñascal-logo.jpg"),

        require("@/assets/Publicidad/Saneamientos_Pereda.jpg"),
        require("@/assets/Publicidad/Serinco.jpg"),
        require("@/assets/Publicidad/Takoa.jpg"),
        require("@/assets/Publicidad/Tempel.jpg"),
        require("@/assets/Publicidad/The_English_Institute.jpg"),

        require("@/assets/Publicidad/2-Pñascal_400x400.png"),
    ],
};