import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);
/* Escribimos las rutas de nuestras paginas */
const routes = [
    { path: "/", component: () =>
            import ("@/components/home/HomePage.vue") },
    {
        path: "/historia",
        component: () =>
            import ("@/components/historia/HistoriaPage.vue"),
    },
    {
        path: "/equipos",
        component: () =>
            import ("@/components/equipos/EquiposPage.vue"),
    },
    {
        path: "/escuela",
        component: () =>
            import ("@/components/escuela/EscuelaPage.vue"),
    },
    {
        path: "/patrocinadores_2",
        component: () =>
            import ("@/components/patrocinadores/PatrocinadoresPage.vue"),
    },
    {
        path: "/contacto",
        component: () =>
            import ("@/components/contacto/Contacto.vue"),
    },
    {
        path: "/partidos",
        component: () =>
            import ("@/components/partidos/PartidosPage.vue"),
    },
    {
        path: "/campus",
        component: () =>
            import ("@/components/campus/CampusPage.vue"),
    },
    {
        path: "/carrussel",
        component: () =>
            import ("@/components/carrussel/Carrussel.vue"),
    },
];

const router = new VueRouter({
    routes,
});

export default router;