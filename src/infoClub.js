export default {
    equipos: {
        eskola: {
            es: {
                categoria: "Escuela de patinaje",
                foto: "Patin-eskola.jpg",
                integrantes: [
                    "De pie:  Haritz Irizar (entrenador); Jorge Niño, Mikel Macías; Xabier García; Iban Martínez; Carlos Celada; Javi Niño (delegado); Eneritz Abendaño; Unai Hierro; ",
                    "Pablo Peña (2 º entrenador); Ainhize Franco; Katalin González; Hodei Martínez (2º entrenador); Aimar Herreros.",
                    "Sentados:  Nico Huerga; Ekhi González; Kimetz Franco; Irati Quevedo; Paule González; Javi; Aritz Del Río; Haizea Lasuen; Amets Mendiolea; Oier Díaz;",
                    "Nahia Rodríguez; Garazi Eibar.",
                ],
            },
            eu: {
                categoria: "Patin eskola",
                foto: "Patin-eskola.jpg",
                integrantes: [
                    "Zutik: Haritz Irizar (entrenatzailea); Jorge Niño, Mikel Macías; Xabier García; Iban Martínez; Carlos Celada; Javi Niño (talde-ordezkaria); Eneritz Abendaño; Unai Hierro; ",
                    "Pablo Peña(2. entrenatzailea);Ainhize Franco;Katalin González;Hodei Martínez(2. entrenatzailea);Aimar Herreros. ",
                    "Jezarrite: Nico Huerga; Ekhi González; Kimetz Franco; Irati Quevedo; Paule González; Javi; Aritz Del Río; Haizea Lasuen; Amets Mendiolea; Oier Díaz;",
                    "Nahia Rodríguez;Garazi Eibar.",
                ],
            },
        },
        benjamin: {
            es: {
                categoria: "Benjamín",
                foto: "Benjamin.jpg",
                integrantes: [
                    "De pie: Rubén Domínguez (2º entrenador); Izan Suco (83); Alba Martínez (58); Hugo Oñate (62); Nagore López (60); Isturitze Aurrekoetxea (entrenadora).",
                    "Sentados: Alain Anta (77); Mateo Cuenca (72); Haidar Ruiz (portero); Jon Tomé (portero); Iñigo Peña (98); Unax Espinosa (91).",
                ],
           },
            eu: {
                categoria: "Benjamína",
                foto: "Benjamin.jpg",
                integrantes: [
                "Zutik: Rubén Domínguez (2. entrenatzailea); Izan Suco (83); Alba Martínez (58); Hugo Oñate (62); Nagore López (60);Isturitze Aurrekoetxea (Entrenatzaile).",
                "Jezarrita: Alain Anta (77); Mateo Cuenca (72); Haidar Ruiz (atezaina); Jon Tomé (atezaina); Iñigo Peña (98); Unax Espinosa (91).",
               ],
            },
          },
        alevinB: {
            es: {
                categoria: "Alevín B",
                foto: "Alevin B.jpg",
                integrantes: [
                    "De pie: Gorka Barroso (entrenador); Aimar Zubia (63); Adrián Ortega (99); Ioritz Fernández (81); Jon Rojo (93); Oier Ortega (2º entrenador).",
                    "Sentados: Gotzon Aguirre (82); Nahia San Juan (54); June Rosillo (portera); Arai Cañas (90); June Ausín (36).",
                ],
            },
            eu: {
                categoria: "Kimua B",
                foto: "Alevin B.jpg",
                integrantes: [
                    "Zutik: Gorka Barroso (entrenatzailea); Aimar Zubia (63); Adrián Ortega (99); Ioritz Fernández (81); Jon Rojo (93); Oier Ortega (2º entrenatzailea).",
                    "Jezarrite: Gotzon Aguirre (82); Nahia San Juan (54); June Rosillo (portress); Arai Cañas (90); June Ausín (36).",
                ],
            },
        },
        alevinA: {
            es: {
                categoria: "Alevín A ",
                foto: "Alevin A.jpg",
                integrantes: [
                    "De pie: Amets Anta (47); Ainhize Martín (72); José Valle (entrenador); Egoitz Zaldibar (73); Eneko Espinosa (40); Endika Arandia (75); Oier Ortega (2º entrenador).",
                    "Sentados: Beñat González (80); Martín Quecedo (85); Asier Zaldibar (portero); Leire Celada (portera); Unax Macías (74); Txaber González (42).",
                ],
            },
            eu: {
                categoria: "Kimua A ",
                foto: "Alevin A.jpg",
                integrantes: [
                    "Zutik: Amets Anta (47); Ainhize Martín (72); José Valle (entrenatzailea); Egoitz Zaldibar (73); Eneko Espinosa (40); Endika Arandia (75);",
                    "Oier Ortega(2 º entrenatzailea).",
                    "Jezarrite: Beñat González (80); Martín Quecedo (85); Asier Zaldibar (atezaina); Leire Celada (portress); Unax Macías (74); Txaber González (42).",
                ],
            },
        },
        infantilB: {
            es: {
                categoria: "Infantil B ",
                foto: "Infantil B.jpg",
                integrantes: [
                    "De pie: Javier Vaamonde (entrenador); Mateo Sancho (71); Anton Sancho (57); Oier Baceta (75); Iker Zaldibar (55); Jara Martinez (45).",
                    "Sentados: Aritz Rosillo (portero); Ainhize Jaureguizar (52); Koldobika Garitagoitia (95); Mateo Gonzalez (38); Iñaki Bereziartua (42).",
                ],
            },
            eu: {
                categoria: "Umea B ",
                foto: "Infantil B.jpg",
                integrantes: [
                    "Zutik: Javier Vaamonde (entrenatzailea); Mateo Sancho (71); Anton Sancho (57); Oier Baceta (75); Iker Zaldibar (55); Jara Martinez (45).",
                    "Jezarrite: Aritz Rosillo (portero); Ainhize Jaureguizar (52); Koldobika Garitagoitia (95); Mateo Gonzalez (38); Iñaki Bereziartua (42).",
                ],
            },
        },
        infantilA: {
            es: {
                categoria: "Infantil A",
                foto: "Infantil A.jpg",
                integrantes: [
                    "De pie: Javier Vaamonde (entrenador); Mateo Murillo (88); Lexuri Apraiz (59); Nora Rojo (43).",
                    "Sentados: Haritz Quintanilla (61); Liher Pérez (10); Aimar Casado (70); Nahia Ausin (32).",
                ],
            },
            eu: {
                categoria: "Umea A",
                foto: "Infantil A.jpg",
                integrantes: [
                    "Zutik: Javier Vaamonde (entrenatzailea); Mateo Murillo (88); Lexuri Apraiz (59); Nora Rojo (43).",
                    "Jezarrite: Haritz Quintanilla (61); Liher Pérez (10); Aimar Casado (70); Nahia Ausin (32).",
                ],
            },
        },

        cadete: {
            es: {
                categoria: "Cadete",
                foto: "Cadete.jpg",
                integrantes: [
                    "De pie: Javier Vaamonde (entrenador); Erik Izquierdo (35); Aitor J. Hierro (30); Ugaitz Peña (34); Lander Casado (28); Anto Cano (delegada).",
                    "Sentados: Arkaitz Rial (portero); Martin Erdozain (37); Egoitz Arandia (29); Ekain Villagra (portero).",
                ],
            },
            eu: {
                categoria: "Kadetea",
                foto: "Cadete.jpg",
                integrantes: [
                    "Zutik: Javier Vaamonde (entrenatzailea); Erik Izquierdo (35); Aitor J. Hierro (30); Ugaitz Peña (34); Lander Casado (28); Anto Cano (taldeko ordezkaria).",
                    "Jezarrite: Arkaitz Rial (atezaina); Martin Erdozain (37); Egoitz Arandia (29); Ekain Villagra (atezaina).",
                ],
            },
        },
        juvenil: {
            es: {
                categoria: "Juvenil ",
                foto: "Juvenil.jpg",
                integrantes: [
                    "De pie: Unai Fernández (entrenador); Josu Hidalgo (69); Oier Ortega (17); Pablo Peña (20); Rubén Dominguez (12); Peru Euskariatza (27).",
                    "Sentados: Ager Cañas (24); Iñigo Ordoñez (portero); Ibai Vio (76); Ander Anguiano (portero); Hodei Martínez (31).",
                ],
            },
            eu: {
                categoria: "Gaztea ",
                foto: "Juvenil.jpg",
                integrantes: [
                    "Zutik: Unai Fernández (entrenatzailea); Josu Hidalgo (69); Oier Ortega (17); Pablo Peña (20); Rubén Dominguez (12); Peru Euskariatza (27).",
                    "Jezarrite: Ager Cañas (24); Iñigo Ordoñez (atezaina); Ibai Vio (76); Ander Anguiano (atezaina); Hodei Martínez (31).",
                ],
            },
        },
        junior: {
            es: {
                categoria: "Junior ",
                foto: "Junior.jpg",
                integrantes: [
                    "De pie: Aitor Méndez (2º entrenador); Eduardo Díez (delegado); Gorka Barroso (19); Aketza García (5); Oier Ortega (17); Karmelo Espina (22),",
                    " Rubén Domínguez (12); Ander Elorduy (entrenador).",
                    "Sentados: Lander Argote (15); Iñigo Ordoñez (portero); Erlantz Ortega (portero); Iker Zarraga (6); Oier Urkullu (16).",
                ],
            },
            eu: {
                categoria: "Juniorra ",
                foto: "Junior.jpg",
                integrantes: [
                    "Zutik: Aitor Méndez (2º entrenatzailea); Eduardo Díez (talde-ordezkaria); Gorka Barroso (19); Aketza García (5); Oier Ortega (17); Karmelo Espina (22),",
                    "Rubén Domínguez(12);Ander Elorduy(entrenatzailea). ",
                    "Jezarrite: Lander Argote (15); Iñigo Ordoñez Martínez (atezaina); Erlantz Ortega (atezaina); Iker Zarraga (6); Oier Urkullu (16).",
                ],
            },
        },
        sub23: {
            es: {
                categoria: "Sub 23 ",
                foto: "Sub 23.jpg",
                integrantes: [
                    "De pie: Aitor Méndez (2º entrenador); Eduardo Díez (delegado); 1 Gorka Barroso (19); Aketza García (5); Isturitze Aurrekoetxea (21); Oier Ortega (17); ",
                    "Karmelo Espina (22), Rubén Domínguez (12); Pablo Peña (20); Ander Elorduy (entrenador).",
                    "Sentados: Hodei Martinez (31); Lander Argote (15); Iñigo Ordoñez (portero); Erlantz Ortega (portero); Unai Fernández (portero); ",
                    "Iker Zarraga(6);Oier Urkullu(16). ",
                ],
            },
            eu: {
                categoria: " 23 azpi ",
                foto: "Sub 23.jpg",
                integrantes: [
                    "Zutik: Aitor Méndez (2º entrenatzailea); Eduardo Díez (talde-ordezkaria); 1 Gorka Barroso (19); Aketza García (5); Isturitze Aurrekoetxea (21); Oier Ortega (17);",
                    "Karmelo Espina(22), Rubén Domínguez(12);Pablo Peña(20); Ander Elorduy(entrenatzailea).",
                    "Jezarrite: Hodei Martinez (31); Lander Argote (15); Iñigo Ordoñez (atezaina); Erlantz Ortega (atezaina); Unai Fernández (atezaina);",
                    "Iker Zarraga(6);Oier Urkullu(16).",
                ],
            },
        },
        senior: {
            es: {
                categoria: "Senior",
                foto: "Senior.jpg",
                integrantes: [
                    "De pie: Aitor Méndez (2º entrenador); Eduardo Díez (delegado); Gorka Barroso (19); Isturitze Aurrekoetxea (21); Oier Ortega (17); Rubén Domínguez (12);",
                    " Endika Alfageme (9); Ander Elorduy (entrenador).",
                    "Sentados: Hegoi Oleaga (23); Unai Fernández (portero); Aketza García (5); Erlantz Ortega (portero); Iker Zarraga (6).",
                ],
            },
            eu: {
                /*   / Seniorra */
                categoria: "Senior",
                foto: "Senior.jpg",
                integrantes: [
                    "Zutik: Aitor Méndez (2º entrenador); Eduardo Díez (delegado); Gorka Barroso (19); Isturitze Aurrekoetxea (21); Oier Ortega (17); Rubén Domínguez (12);",
                    "Endika Alfageme(9); Ande Elorduy(entrenatzailea). ",
                    "Jezarrite: Hegoi Oleaga (23); Unai Fernández (atezaina); Aketza García (5); Erlantz Ortega (atezaina); Iker Zarraga (6).",
                ],
            },
        },
        femenino: {
            es: {
                categoria: "Femenino",
                foto: "Femenino.jpg",
                integrantes: [
                    "De pie: José Anta (entrenador); Lexuri Apraiz (59); Arai Cañas (90); Ainhize Martín (72); Oihane Barreras (13); Ainhize Jaureguizar (52); Nora Rojo (43);",
                    " Jara Martinez (45); Isturitze Aurrekoetxea (2º entrenadora).",
                    "Sentadas: Alba Martínez (58); Nahia San Juan (77); Leire Celada (portera); June Rosillo (portera); June Ausín (36); Nagore López (60).",
                ],
            },
            eu: {
                categoria: "Neskak",
                foto: "Femenino.jpg",
                integrantes: [
                    "Zutik: José Anta (entrenatzailea); Lexuri Apraiz (59); Arai Cañas (90); Ainhize Martín (72); Oihane Barreras (13); Ainhize Jaureguizar (52); Nora Rojo (43);",
                    "Jara Martinez(45);  Aurrekoetxea(2. entrenatzailea).",
                    "Jezarrita: Alba Martínez (58); Nahia San Juan (77); Leire Celada (portress); June Rosillo (portress); June Ausín (36); Nagore López (60).",
                ],
            },
        },
        izaharrak: {
            es: {
                categoria: "Veteranos ",
                foto: "IZaharrak 1.jpeg",
                integrantes: [
                    "De pie: Arkaitz Herreros; Iker Viteri; Aitor Arostegi; Jose Luís Pichel; Sergio Macías; Jose Luis Valle.",
                    "Sentados: Arkaitz Garai; Vicente Moralejo (portero); Igor Lamikiz (portero); Joseba Gotzon Martín (portero); Jose Andrés Torrealday.",
                ],
            },
            eu: {
                categoria: " Izaharrak ",
                foto: "IZaharrak 1.jpeg",
                integrantes: [
                    "Zutik: Arkaitz Herreros; Iker Viteri; Aitor Arostegi; Jose Luís Pichel; Sergio Macías; Jose Luis Valle.",
                    "Jezarrita: Arkaitz Garai; Vicente Moralejo (atezaina); Igor Lamikiz (atezaina); Joseba Gotzon Martín (atezaina); Jose Andrés Torrealday.",
                ],
            },
        },
    },
};