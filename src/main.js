//import Vue from "vue";
//import App from "./App.vue";
//import ElementUI from "element-ui";
//import "element-ui/lib/theme-chalk/index.css";
//Vue.use(ElementUI);
//Vue.config.productionTip = false;
//new Vue({
//  el: "#app",
//  render: h => h(App)
//}).$mount("#app");

import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import VueCarousel from "vue-carousel";
Vue.use(VueCarousel);
Vue.config.productionTip = false;
import VueCookies from 'vue-cookies'
Vue.use(VueCookies)
new Vue({
    router,
    render: (h) => h(App),
}).$mount("#app");